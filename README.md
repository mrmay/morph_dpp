# Morphological partitioning using the Dirichlet Process Prior (DPP)

## Simulations

Simulate data under a few scenarios:
- Simulate 1: under a uniform model (all characters have same rate/process).
- [skip]Simulate 2: with 3 rate categories (low, medium, high).
- [skip]Simulate 3: with 3 process categories (even, biased, very biased).
- Simulate 4: with 3 rate categories and 3 process categories. 
- Simulate 5: under a DPP. -> with expected number of categories = 3?

For each scenario, simulate x trees with n = {n_1, n_2, n_3} tips.
**(n to be determined. We should simulate under an FBD, but will have to think about how to specify parameters, and whether we can simulate trees with fixed number of tips.)**
Rescale the trees to have length of 1 (~1 change per character, which will get multiplied by the true rate parameter).
For each tree, simulate a morphological dataset under the specified partition model.
**(Also simulate a molecular alignment for when we infer the tree w/ extinct and extant tips.)**
**+separate rate & process (only either one is under DPP)
    


Things to think about: how many tips? how many characters? what are the true rate parameters?

### Analysis 1:

Analyze morphologial data on a fixed tree (no molecular data) under the DPP model.

Q: How well can we infer the true model?

### Analysis 2

Estimate the tree (with molecular data for extants) under the true model and with the DPP. 

Q1: How well can we infer the true model under the DPP when the tree is unknown? 

Q2: How much does the model matter to estimates of topology and branch length?

## Empirical analysis

##add random 
