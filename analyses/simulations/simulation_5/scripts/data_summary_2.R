# find all the files
all_files <- list.files("data/", recursive = TRUE, full.names = TRUE)

# get all files for a given dataset size
files_50  <- grep("params_50.txt", all_files, value = TRUE)
files_100 <- grep("params_100.txt", all_files, value = TRUE)
files_200 <- grep("params_200.txt", all_files, value = TRUE)

#  compute the number of clusters
num_50 <- do.call(rbind, lapply(files_50, function(x) {
  ll <- readLines(x)
  nr <- as.numeric(ll[1])
  np <- as.numeric(ll[3])
  return(c(nr, np))
}))

num_100 <- do.call(rbind, lapply(files_100, function(x) {
  ll <- readLines(x)
  nr <- as.numeric(ll[1])
  np <- as.numeric(ll[3])
  return(c(nr, np))
}))

num_200 <- do.call(rbind, lapply(files_200, function(x) {
  ll <- readLines(x)
  nr <- as.numeric(ll[1])
  np <- as.numeric(ll[3])
  return(c(nr, np))
}))

mean(num_50)
mean(num_100)
mean(num_200)



