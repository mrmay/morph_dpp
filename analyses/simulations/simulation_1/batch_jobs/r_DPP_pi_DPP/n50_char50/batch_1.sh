#!/bin/bash
#SBATCH --partition=savio
#SBATCH --account=fc_rothfelslab
#SBATCH --qos=savio_normal
#SBATCH --mail-user=eriawei@berkeley.edu
#SBATCH --mail-type=ALL
#SBATCH --time=24:00:00


# load dependencies
module load rb

# change to the repo directory
cd /global/scratch/users/eriawei/morph_dpp/analyses/simulations/simulation_1/

rb jobs/job_n_50_tree_1_char_50_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_50_tree_2_char_50_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_50_tree_3_char_50_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_50_tree_4_char_50_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_50_tree_5_char_50_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_50_tree_6_char_50_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_50_tree_7_char_50_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_50_tree_8_char_50_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_50_tree_9_char_50_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_50_tree_10_char_50_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_50_tree_11_char_50_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_50_tree_12_char_50_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_50_tree_13_char_50_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_50_tree_14_char_50_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_50_tree_15_char_50_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_50_tree_16_char_50_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_50_tree_17_char_50_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_50_tree_18_char_50_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_50_tree_19_char_50_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_50_tree_20_char_50_r_DPP_pi_DPP.Rev ;

wait;


