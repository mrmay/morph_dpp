#!/bin/bash
#SBATCH --partition=savio
#SBATCH --account=fc_rothfelslab
#SBATCH --qos=savio_normal
#SBATCH --mail-user=eriawei@berkeley.edu
#SBATCH --mail-type=ALL
#SBATCH --time=24:00:00


# load dependencies
module load rb

# change to the repo directory
cd /global/scratch/users/eriawei/morph_dpp/analyses/simulations/simulation_1/

rb jobs/job_n_100_tree_41_char_100_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_100_tree_42_char_100_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_100_tree_43_char_100_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_100_tree_44_char_100_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_100_tree_45_char_100_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_100_tree_46_char_100_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_100_tree_47_char_100_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_100_tree_48_char_100_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_100_tree_49_char_100_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_100_tree_50_char_100_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_100_tree_51_char_100_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_100_tree_52_char_100_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_100_tree_53_char_100_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_100_tree_54_char_100_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_100_tree_55_char_100_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_100_tree_56_char_100_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_100_tree_57_char_100_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_100_tree_58_char_100_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_100_tree_59_char_100_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_100_tree_60_char_100_r_DPP_pi_DPP.Rev ;

wait;


