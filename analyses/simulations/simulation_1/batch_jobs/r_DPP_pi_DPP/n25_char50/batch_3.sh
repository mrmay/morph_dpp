#!/bin/bash
#SBATCH --partition=savio
#SBATCH --account=fc_rothfelslab
#SBATCH --qos=savio_normal
#SBATCH --mail-user=eriawei@berkeley.edu
#SBATCH --mail-type=ALL
SBATCH --time=72:00:00


# load dependencies
module load rb

# change to the repo directory
cd /global/scratch/users/eriawei/morph_dpp/analyses/simulations/simulation_1/

rb jobs/job_n_25_tree_41_char_50_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_25_tree_42_char_50_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_25_tree_43_char_50_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_25_tree_44_char_50_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_25_tree_45_char_50_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_25_tree_46_char_50_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_25_tree_47_char_50_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_25_tree_48_char_50_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_25_tree_49_char_50_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_25_tree_50_char_50_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_25_tree_51_char_50_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_25_tree_52_char_50_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_25_tree_53_char_50_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_25_tree_54_char_50_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_25_tree_55_char_50_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_25_tree_56_char_50_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_25_tree_57_char_50_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_25_tree_58_char_50_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_25_tree_59_char_50_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_25_tree_60_char_50_r_DPP_pi_DPP.Rev ;

wait;


