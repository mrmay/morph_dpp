#!/bin/bash
#SBATCH --partition=savio
#SBATCH --account=fc_rothfelslab
#SBATCH --qos=savio_normal
#SBATCH --mail-user=eriawei@berkeley.edu
#SBATCH --mail-type=ALL
#SBATCH --time=24:00:00


# load dependencies
module load rb

# change to the repo directory
cd /global/scratch/users/eriawei/morph_dpp/analyses/simulations/simulation_1/

rb jobs/job_n_200_tree_21_char_200_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_200_tree_22_char_200_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_200_tree_23_char_200_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_200_tree_24_char_200_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_200_tree_25_char_200_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_200_tree_26_char_200_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_200_tree_27_char_200_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_200_tree_28_char_200_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_200_tree_29_char_200_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_200_tree_30_char_200_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_200_tree_31_char_200_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_200_tree_32_char_200_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_200_tree_33_char_200_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_200_tree_34_char_200_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_200_tree_35_char_200_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_200_tree_36_char_200_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_200_tree_37_char_200_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_200_tree_38_char_200_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_200_tree_39_char_200_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_200_tree_40_char_200_r_DPP_pi_DPP.Rev ;

wait;


