#!/bin/bash
#SBATCH --partition=savio
#SBATCH --account=fc_rothfelslab
#SBATCH --qos=savio_normal
#SBATCH --mail-user=eriawei@berkeley.edu
#SBATCH --mail-type=ALL
#SBATCH --time=24:00:00


# load dependencies
module load rb

# change to the repo directory
cd /global/scratch/users/eriawei/morph_dpp/analyses/simulations/simulation_1/

rb jobs/job_n_200_tree_61_char_200_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_200_tree_62_char_200_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_200_tree_63_char_200_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_200_tree_64_char_200_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_200_tree_65_char_200_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_200_tree_66_char_200_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_200_tree_67_char_200_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_200_tree_68_char_200_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_200_tree_69_char_200_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_200_tree_70_char_200_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_200_tree_71_char_200_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_200_tree_72_char_200_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_200_tree_73_char_200_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_200_tree_74_char_200_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_200_tree_75_char_200_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_200_tree_76_char_200_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_200_tree_77_char_200_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_200_tree_78_char_200_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_200_tree_79_char_200_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_200_tree_80_char_200_r_DPP_pi_DPP.Rev ;

wait;


