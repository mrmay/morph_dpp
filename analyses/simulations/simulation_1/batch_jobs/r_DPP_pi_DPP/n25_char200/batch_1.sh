#!/bin/bash
#SBATCH --partition=savio
#SBATCH --account=fc_rothfelslab
#SBATCH --qos=savio_normal
#SBATCH --mail-user=eriawei@berkeley.edu
#SBATCH --mail-type=ALL
#SBATCH --time=24:00:00


# load dependencies
module load rb

# change to the repo directory
cd /global/scratch/users/eriawei/morph_dpp/analyses/simulations/simulation_1/

rb jobs/job_n_25_tree_1_char_200_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_25_tree_2_char_200_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_25_tree_3_char_200_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_25_tree_4_char_200_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_25_tree_5_char_200_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_25_tree_6_char_200_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_25_tree_7_char_200_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_25_tree_8_char_200_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_25_tree_9_char_200_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_25_tree_10_char_200_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_25_tree_11_char_200_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_25_tree_12_char_200_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_25_tree_13_char_200_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_25_tree_14_char_200_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_25_tree_15_char_200_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_25_tree_16_char_200_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_25_tree_17_char_200_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_25_tree_18_char_200_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_25_tree_19_char_200_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_25_tree_20_char_200_r_DPP_pi_DPP.Rev ;

wait;


