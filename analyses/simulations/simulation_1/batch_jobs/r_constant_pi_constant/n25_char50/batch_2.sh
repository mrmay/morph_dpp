#!/bin/bash
#SBATCH --partition=savio
#SBATCH --account=fc_rothfelslab
#SBATCH --qos=savio_normal
#SBATCH --mail-user=eriawei@berkeley.edu
#SBATCH --mail-type=ALL
#SBATCH --time=72:00:00


# load dependencies
module load rb

# change to the repo directory
cd /global/scratch/users/eriawei/morph_dpp/analyses/simulations/simulation_1/

rb jobs/job_n_25_tree_21_char_50_r_constant_pi_constant.Rev & 
rb jobs/job_n_25_tree_22_char_50_r_constant_pi_constant.Rev & 
rb jobs/job_n_25_tree_23_char_50_r_constant_pi_constant.Rev & 
rb jobs/job_n_25_tree_24_char_50_r_constant_pi_constant.Rev & 
rb jobs/job_n_25_tree_25_char_50_r_constant_pi_constant.Rev & 
rb jobs/job_n_25_tree_26_char_50_r_constant_pi_constant.Rev & 
rb jobs/job_n_25_tree_27_char_50_r_constant_pi_constant.Rev & 
rb jobs/job_n_25_tree_28_char_50_r_constant_pi_constant.Rev & 
rb jobs/job_n_25_tree_29_char_50_r_constant_pi_constant.Rev & 
rb jobs/job_n_25_tree_30_char_50_r_constant_pi_constant.Rev & 
rb jobs/job_n_25_tree_31_char_50_r_constant_pi_constant.Rev & 
rb jobs/job_n_25_tree_32_char_50_r_constant_pi_constant.Rev & 
rb jobs/job_n_25_tree_33_char_50_r_constant_pi_constant.Rev & 
rb jobs/job_n_25_tree_34_char_50_r_constant_pi_constant.Rev & 
rb jobs/job_n_25_tree_35_char_50_r_constant_pi_constant.Rev & 
rb jobs/job_n_25_tree_36_char_50_r_constant_pi_constant.Rev & 
rb jobs/job_n_25_tree_37_char_50_r_constant_pi_constant.Rev & 
rb jobs/job_n_25_tree_38_char_50_r_constant_pi_constant.Rev & 
rb jobs/job_n_25_tree_39_char_50_r_constant_pi_constant.Rev & 
rb jobs/job_n_25_tree_40_char_50_r_constant_pi_constant.Rev ;

wait;


