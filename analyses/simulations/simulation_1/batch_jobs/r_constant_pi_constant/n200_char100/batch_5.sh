#!/bin/bash
#SBATCH --partition=savio
#SBATCH --account=fc_rothfelslab
#SBATCH --qos=savio_normal
#SBATCH --mail-user=eriawei@berkeley.edu
#SBATCH --mail-type=ALL
#SBATCH --time=24:00:00


# load dependencies
module load rb

# change to the repo directory
cd /global/scratch/users/eriawei/morph_dpp/analyses/simulations/simulation_1/

rb jobs/job_n_200_tree_81_char_100_r_constant_pi_constant.Rev & 
rb jobs/job_n_200_tree_82_char_100_r_constant_pi_constant.Rev & 
rb jobs/job_n_200_tree_83_char_100_r_constant_pi_constant.Rev & 
rb jobs/job_n_200_tree_84_char_100_r_constant_pi_constant.Rev & 
rb jobs/job_n_200_tree_85_char_100_r_constant_pi_constant.Rev & 
rb jobs/job_n_200_tree_86_char_100_r_constant_pi_constant.Rev & 
rb jobs/job_n_200_tree_87_char_100_r_constant_pi_constant.Rev & 
rb jobs/job_n_200_tree_88_char_100_r_constant_pi_constant.Rev & 
rb jobs/job_n_200_tree_89_char_100_r_constant_pi_constant.Rev & 
rb jobs/job_n_200_tree_90_char_100_r_constant_pi_constant.Rev & 
rb jobs/job_n_200_tree_91_char_100_r_constant_pi_constant.Rev & 
rb jobs/job_n_200_tree_92_char_100_r_constant_pi_constant.Rev & 
rb jobs/job_n_200_tree_93_char_100_r_constant_pi_constant.Rev & 
rb jobs/job_n_200_tree_94_char_100_r_constant_pi_constant.Rev & 
rb jobs/job_n_200_tree_95_char_100_r_constant_pi_constant.Rev & 
rb jobs/job_n_200_tree_96_char_100_r_constant_pi_constant.Rev & 
rb jobs/job_n_200_tree_97_char_100_r_constant_pi_constant.Rev & 
rb jobs/job_n_200_tree_98_char_100_r_constant_pi_constant.Rev & 
rb jobs/job_n_200_tree_99_char_100_r_constant_pi_constant.Rev & 
rb jobs/job_n_200_tree_100_char_100_r_constant_pi_constant.Rev ;

wait;


