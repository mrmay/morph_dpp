#!/bin/bash
#SBATCH --partition=savio
#SBATCH --account=fc_rothfelslab
#SBATCH --qos=savio_normal
#SBATCH --mail-user=eriawei@berkeley.edu
#SBATCH --mail-type=ALL
#SBATCH --time=72:00:00


# load dependencies
module load rb

# change to the repo directory
cd /global/scratch/users/eriawei/morph_dpp/analyses/simulations/jobs/


rb jobs/job_n_100_tree_62_char_100_r_constant_pi_constant.Rev & 
rb jobs/job_n_100_tree_62_char_100_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_50_tree_62_char_100_r_constant_pi_constant.Rev & 
rb jobs/job_n_50_tree_62_char_100_r_DPP_pi_DPP.Rev & 
rb jobs/job_n_25_tree_62_char_100_r_constant_pi_constant.Rev & 
rb jobs/job_n_25_tree_62_char_100_r_DPP_pi_DPP.Rev ;  


wait;


