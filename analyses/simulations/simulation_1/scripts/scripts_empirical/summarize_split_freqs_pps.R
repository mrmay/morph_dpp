# run from analyses/empirical/wright
# setwd("analyses/simulations/simulation_1")

library(ape)
library(parallel)
library(viridis)
library(ggplot2)

# the comparison plot
compareXbyY <- function(x, y, col = "black", threshold = 0.25, ...) {
  
  # drop everything that doesn't meet threshold
  # on at least one axis
  retain <- x >= threshold | y >= threshold
  x <- x[retain]
  y <- y[retain]
  col <- col[retain]
  
  # make scatterplot
  points(x, y, col = col, ...)
  
}

# read the PPS summary
pps_table <- read.table("model_comparison/pps_summary.tsv", header = TRUE, stringsAsFactors = FALSE, sep = "\t")

# remove datasets that DPP failed for
pps_table   <- pps_table[pps_table$dpp_fail < 0.95,]

# get the tsv files
split_files  <- list.files(recursive = TRUE, pattern = "splits.tsv", full.names = FALSE)

# only use the datasets that we have PPS for
split_files <- split_files[do.call(rbind, strsplit(split_files, "/"))[,2] %in% pps_table$dataset]

# read all the tsvs
split_tables <- lapply(split_files, function(x) read.table(x, sep = "\t", stringsAsFactors = FALSE, header = TRUE))

# drop the first row from each
split_tables <- lapply(split_tables, function(x) x[-1,])

# name the split tables
names(split_tables) <- do.call(rbind, strsplit(split_files, "/"))[,2]

# combine split tables
splits_combined <- do.call(rbind, split_tables)

# splits by model
splits_constant <- splits_combined[,1:4]
colnames(splits_constant) <- paste0("run ", 1:4)

splits_DPP <- splits_combined[,4 + 1:4]
colnames(splits_DPP) <- paste0("run ", 1:4)

splits_finite <- splits_combined[,8 + 1:4]
colnames(splits_finite) <- paste0("run ", 1:4)

# splits combined across run
splits_averaged <- data.frame(constant = rowMeans(splits_constant),
                              finite   = rowMeans(splits_finite),
                              DPP      = rowMeans(splits_DPP),
                              check.names = FALSE)

#############################################################
# now color by model adequacy under the constant-rate model #
#############################################################

# try with ggplot
splits_averaged_tmp <- splits_averaged
splits_averaged_tmp$inadequacy <- rep(pps_table$constant_fail, times = sapply(split_tables, nrow))

# shuffle
splits_averaged_tmp <- splits_averaged_tmp[sample.int(nrow(splits_averaged_tmp)),]

# filter
threshold <- 0.1

p <- ggplot(splits_averaged_tmp, aes(x = constant, y = DPP, col = inadequacy)) +
  geom_abline(slope = 1, intercept = 0, size = 0.5, linetype = "dashed") +
  geom_point(data = subset(splits_averaged_tmp, constant > threshold | DPP > threshold), size = 1) +
  scale_color_viridis(option = "plasma", end = 0.7) +
  scale_x_continuous(breaks = seq(0, 1, 0.1), expand = c(0.01,0.01)) +
  scale_y_continuous(breaks = seq(0, 1, 0.1), expand = c(0.01,0.01)) +
  theme(legend.position = c(0.1, 0.975), legend.justification = "top")

pdf("figures/split_freqs_by_adequacy.pdf")
plot(p)
dev.off()

# ggplot(splits_averaged_tmp, aes(x = constant, y = DPP, col = inadequacy > 0.9)) +
#   geom_abline(slope = 1, intercept = 0, size = 0.5, linetype = "dashed") +
#   geom_point(data = subset(splits_averaged_tmp, constant > threshold | DPP > threshold), size = 1) +
#   scale_x_continuous(breaks = seq(0, 1, 0.1), expand = c(0.01,0.01)) +
#   scale_y_continuous(breaks = seq(0, 1, 0.1), expand = c(0.01,0.01)) +
#   theme(legend.position = c(0.1, 0.975), legend.justification = "top")

##############################
# sum of squared differences #
##############################

# # for each dataset, compute the normalized sum of squared differences in posteriors
# ssd_df <- vector("list", length(split_tables))
# for(i in 1:length(split_tables)) {
#   
#   # get the table
#   this_table <- split_tables[[i]]
#   
#   # compute averages
#   this_table_constant <- rowMeans(this_table[, 1:4])
#   this_table_dpp      <- rowMeans(this_table[, 4 + 1:4])
#   this_table_finite   <- rowMeans(this_table[, 8 +1:4])
#  
#   # compute the distance
#   this_constant_vs_dpp    <- sum((this_table_constant - this_table_dpp)^2)
#   this_constant_vs_finite <- sum((this_table_constant - this_table_finite)^2)
#   this_dpp_vs_finite      <- sum((this_table_dpp - this_table_finite)^2)
#   
#   # normalize
#   # this_constant_vs_dpp    <- this_constant_vs_dpp / nrow(this_table)
#   # this_constant_vs_finite <- this_constant_vs_finite / nrow(this_table)
#   # this_dpp_vs_finite      <- this_dpp_vs_finite / nrow(this_table)
#  
#   # store
#   ssd_df[[i]] <- data.frame(dataset            = pps_table$dataset[i],
#                             constant_vs_dpp    = this_constant_vs_dpp,
#                             constant_vs_finite = this_constant_vs_finite,
#                             dpp_vs_finite      = this_dpp_vs_finite,
#                             constant_adequacy  = pps_table$constant_fail[i],
#                             finite_adequacy    = pps_table$finite_fail[i],
#                             dpp_adequacy       = pps_table$dpp_fail[i])
#     
# }
# ssd_df <- do.call(rbind, ssd_df)
# 
# ggplot(ssd_df, aes(x = constant_adequacy, y = constant_vs_dpp)) +
#   geom_point()





