#!/bin/bash
#SBATCH --partition=savio
#SBATCH --account=fc_rothfelslab
#SBATCH --qos=savio_normal
#SBATCH --mail-user=eriawei@berkeley.edu
#SBATCH --mail-type=ALL
#SBATCH --time=72:00:00


# load dependencies
module load rb

# change to the repo directory
cd /global/scratch/users/eriawei/morph_dpp/analyses/simulations/jobs/

rb jobs/JOB1.Rev & 
rb jobs/JOB2.Rev & 
rb jobs/JOB3.Rev & 
rb jobs/JOB4.Rev & 
rb jobs/JOB5.Rev & 
rb jobs/JOB6.Rev & 
rb jobs/JOB7.Rev & 
rb jobs/JOB8.Rev & 
rb jobs/JOB9.Rev & 
rb jobs/JOB10.Rev & 
rb jobs/JOB11.Rev & 
rb jobs/JOB12.Rev & 
rb jobs/JOB13.Rev & 
rb jobs/JOB14.Rev & 
rb jobs/JOB15.Rev & 
rb jobs/JOB16.Rev & 
rb jobs/JOB17.Rev & 
rb jobs/JOB18.Rev & 
rb jobs/JOB19.Rev & 
rb jobs/JOB20.Rev & 


wait;


