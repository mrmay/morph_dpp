#Compare estimated rate & pi to true values simulated under a fixed number category

#setwd("morph_dpp/analyses/simulations/simulation_4")

#-------------------- 定义函数 --------------------#
# define a function to get Parameter per character #

getParamsPerCharacter <- function(parameters, allocations) {
  
  # separate pi from rate
  rate_allocations <- allocations[,grepl("site_rate", colnames(allocations))]
  pi_allocations   <- allocations[,grepl("site_matrix", colnames(allocations))]
  
  # compute the number of tables
  rate_num_tables  <- apply(rate_allocations, 1, function(x) length(unique(x)))
  pi_num_tables    <- apply(pi_allocations, 1, function(x) length(unique(x)))
  
  # get rates per table
  rate_params      <- parameters[, grep("rate\\[", colnames(parameters))]
  pi_params        <- parameters[, grep("pi\\[", colnames(parameters))]
  
  # flatten pi and rates
  flat_pi          <- as.vector(t(pi_params))
  flat_rate        <- as.vector(t(rate_params))
  flat_pi_alloc    <- as.vector(t(pi_allocations))
  flat_rate_alloc  <- as.vector(t(rate_allocations))
  
  # get pi and rate per character
  num_samples        <- nrow(parameters)
  num_characters     <- ncol(rate_allocations)
  num_tables         <- ncol(rate_params)
  flat_pi_per_char   <- flat_pi[flat_pi_alloc     + rep(num_tables * (1:num_samples - 1), 
                                                        each = num_characters)]
  flat_rate_per_char <- flat_rate[flat_rate_alloc + rep(num_tables * (1:num_samples - 1), 
                                                        each = num_characters)]
  
  # make them into a matrix
  pi_per_char        <- matrix(flat_pi_per_char, 
                               nrow = num_samples, byrow = TRUE)
  rate_per_char      <- matrix(flat_rate_per_char, 
                               nrow = num_samples, byrow = TRUE)
  
  # return those matrices
  res <- list(pi              = pi_per_char, 
              rate            = rate_per_char,
              pi_num_tables   = pi_num_tables,
              rate_num_tables = rate_num_tables)
  return(res)
  
}


#-------------------- 基本设定 --------------------#
library(ape)

# settings
n          <- c(25, 50, 100, 200)
c          <- c(50, 100, 200)
rate_model <- c("DPP") #change model
pi_model   <- c("DPP") #change model
tree       <- 1:100
num_transitions <- 5

# expand all combinations
grid <- expand.grid(n                =  n, 
                    c                =  c, 
                    rate_model       =  rate_model, 
                    pi_model         =  pi_model, 
                    tree             =  tree, 
                    stringsAsFactors =  FALSE)
grid <- grid[grid$rate_model         == grid$pi_model,]

grid[grid$n == 25 & grid$c == 50 & grid$tree == 24,]

#-------------------- 定义真值 ----------------------#

tree_lengths <- read.table("data/tree_lengths.tsv", header=TRUE)

# # true values
# true_pi <- 0.75
# true_rate  <- num_transitions / 1

#------------------- BOXPLOTs for each dtset --------------------#

results <- vector("list", length = nrow(grid))

# loop 1: get params_per_char - all combinations
bar <- txtProgressBar(style = 3, width = 40)
for(i in 1:nrow(grid)) {
  
  setTxtProgressBar(bar, i / nrow(grid))
  
  # get the current combination
  this_analysis   <- grid[i,]
  this_n          <- this_analysis$n
  this_c          <- this_analysis$c
  this_rate_model <- this_analysis$rate_model
  this_pi_model   <- this_analysis$pi_model
  this_tree       <- this_analysis$tree
  
  # read the true tree
  tree_fn <- paste0("data/n_", this_n, 
                    "/tree_",  this_tree, 
                    "/tree.tre")
  tree    <- read.tree(tree_fn)
  
  # read the samples
  output_params <- paste0("output/n_",            this_n, 
                          "/char_",               this_c, 
                          "/rate_",               rate_model, 
                          "_pi_",                 pi_model, 
                          "_tree_unrooted/tree_", this_tree, 
                          "/params_run_1.log")
  output_allocs <- paste0("output/n_",            this_n, 
                          "/char_",               this_c, 
                          "/rate_",               rate_model, 
                          "_pi_",                 pi_model, 
                          "_tree_unrooted/tree_", this_tree, 
                          "/allocation_run_1.log")
  params <- read.table(output_params, 
                       header = TRUE, stringsAsFactors = FALSE, check.names = FALSE)
  allocs <- read.table(output_allocs, 
                       header = TRUE, stringsAsFactors = FALSE, check.names = FALSE)
  
  # hack for constant pi model
  if ( "pi[1]" %in% colnames(params) == FALSE ) {
    params["pi[2]"] <- params["pi[1]"] <- params$global_pi
  }
  
  # get per-character parameter estimates
  params_per_char <- getParamsPerCharacter(params, allocs)
  
  # get per-character true parameters !真值!
  true_param_fn     <- paste0("data/n_", this_n, "/tree_", this_tree,"/params_", this_c,".txt")
  true_param_string <- readLines(true_param_fn, warn = FALSE)
  
  # true RATES 
  true_param_rate <- true_param_string[2]
  true_param_rate <- gsub("[", "", true_param_rate, fixed = TRUE)
  true_param_rate <- gsub("]", "", true_param_rate, fixed = TRUE)
  true_param_rate <- gsub(" ", "", true_param_rate, fixed = TRUE)
  true_param_rate <- as.numeric(strsplit(true_param_rate, ",")[[1]])
  this_tl         <- tree_lengths$tl[tree_lengths$ntaxa == this_n]
  true_param_rate <- true_param_rate * this_tl / (this_tl / sum(tree$edge.length))
  
  # true PI
  true_param_pi <- true_param_string[4]
  true_param_pi <- gsub("[", "", true_param_pi, fixed = TRUE)
  true_param_pi <- gsub("]", "", true_param_pi, fixed = TRUE)
  true_param_pi <- gsub(" ", "", true_param_pi, fixed = TRUE)
  true_param_pi <- as.numeric(strsplit(true_param_pi, ",")[[1]])
  
  # true_param_pi
  # colMeans(params_per_char$pi)
  
  # write some summaries 总结哪些项
  output_dir <- paste0("output/n_",            this_n, 
                       "/char_",               this_c, 
                       "/rate_",               rate_model, 
                       "_pi_",                 pi_model, 
                       "_tree_unrooted/tree_", this_tree)
  
  # plots !制图!
  pdf(paste0(output_dir, "/param_boxplots.pdf"), width = 12)
    par(mfrow = c(1,2))
    #PI boxplot
    boxplot(params_per_char$pi, 
            main     = paste0("pi, ",    pi_model, 
                              "(n=",     this_n, 
                              ", char=", this_c, ")"),
            cex.main = 1,
            outline  = FALSE, 
            ylim     = c(0,1))
    points(true_param_pi, 
           col       = "red", 
           pch       = 19)
    #RATE boxplot
    boxplot(params_per_char$rate,
            main     = paste0("rate, ",  rate_model, 
                              "(n=",     this_n, 
                              ", char=", this_c, ")"),
            cex.main = 1,
            outline  = FALSE, 
            ylim     = c(0,5))
    points(true_param_rate,
           col       = "blue", 
           pch       = 19)
  dev.off()

#-------------------- 总结 ------------------------#
  # summary statistics for 【character-specific】 estimates
  
  # PI coverage
  pi_quants  <- apply(params_per_char$pi, 2, quantile, prob = c(0.025, 0.975))
  pi_covered <- true_param_pi > pi_quants[1,] & true_param_pi < pi_quants[2,] 
  # RATE coverage
  rate_quants  <- apply(params_per_char$rate, 2, quantile, prob = c(0.025, 0.975))
  rate_covered <- true_param_rate > rate_quants[1,] & true_param_rate < rate_quants[2,] 
  
  # PI mean squared error （MSE） of posterior
  pi_MSE   <- (colMeans(params_per_char$pi) - true_param_pi)^2
  # RATE MSE of posterior
  rate_MSE <- (colMeans(params_per_char$rate) - true_param_rate)^2
  
  # summary statistics for 【number of partitions】of each dtset.
    # PI average number of partitions
  pi_avg_tbl   <- mean(params_per_char$pi_num_tables)
    # RATE average number of partitions
  rate_avg_tbl <- mean(params_per_char$rate_num_tables)
  
  #Write a dataset-specific summary table for PI
  pi_data_frame = data.frame(pi_covered            = pi_covered,
                             pi_mean_squared_error = pi_MSE,
                             pi_quantiles          = pi_quants) 
  
  results_pi      <- vector("list", length = nrow(grid))
  results_pi[[i]] <- pi_data_frame  
  results_pi      <- do.call(rbind, results_pi)
  
  write.table(results_pi, 
              file = paste0("output/n_",              this_n, 
                            "/char_",                 this_c, 
                            "/rate_",                 rate_model,
                            "_pi_",                   pi_model,
                            "_tree_unrooted/tree_",   this_tree,
                            "/PI_coverage_summary(c", this_c,
                            "_n",                     this_n,
                            ").tsv"), 
                            sep = "\t", quote = FALSE, row.names = FALSE)
  
  
  #Write a dataset-specific summary table for RATE
  rate_data_frame = data.frame(rate_covered            = rate_covered,
                               rate_mean_squared_error = rate_MSE,
                               rate_quantiles          = rate_quants) 
  
  results_rate      <- vector("list", length = nrow(grid))
  results_rate[[i]] <- rate_data_frame
  results_rate      <- do.call(rbind, results_rate)
  
  write.table(results_rate, 
              file = paste0("output/n_",                this_n, 
                            "/char_",                   this_c, 
                            "/rate_",                   rate_model,
                            "_pi_",                     pi_model,
                            "_tree_unrooted/tree_",     this_tree,
                            "/RATE_coverage_summary(c", this_c,
                            "_n",                       this_n,
                            ").tsv"), 
                            sep = "\t", quote = FALSE, row.names = FALSE)
  
  
  # WHAT IS THIS (commenting out for now.)
  #write.table(results_pi, 
  #            file = paste0("data/data_summary/", rate_model, "_coverage_summary.tsv"), 
  #                   sep = "\t", quote = FALSE, row.names = FALSE)
  
  

  # quantiles for number of partitions
  quantile(params_per_char$pi_num_tables, prob = c(0.025, 0.975))
  # repeat for rates
  # make a table
  
  # replace this_c w/ the true value for whatever simulation (e.g. get the true value from the dpp estimates.)
  # pdf(paste0("data/data_summary/", pi_model, "_coverage_summary_pi.pdf"))
  #   boxplot(params_per_char$pi)
  #   points(rep(0.75, this_c), col = "red", pch = 19)
  #   mean((colMeans(params_per_char$pi) - rep(0.75, this_c))^2)
  # dev.off()
  
  ####################################################################
  ####################################################################
  
  ## loop2-1: compute for pi - each character
  
  pi_summary <- vector("list", length = ncol(params_per_char$pi))
  
  for(j in 1:ncol(params_per_char$pi)) {
    
    # 1a.  average coverage per character (averaged over characters)
    pi_interval <- quantile(params_per_char$pi[,j], prob=c(0.025,0.975))
    whether_pi_is_contained <- true_pi>pi_interval[1] & true_pi<pi_interval[2] 
    
    # 1d. coefficient of Variation
    pi_cv <- sd(params_per_char$pi[,j]) / mean(params_per_char$pi[,j])
    
    pi_dtframe = data.frame(pi_coverage = whether_pi_is_contained, pi_coefficient_of_variation = pi_cv)
    
    pi_summary[[j]] = pi_dtframe
  }
  
  pi_summary <- do.call(rbind, pi_summary)
  
  
  # 1b.  average percent error per character (averaged over characters)
  pi_avg_perc_err <- mean(abs((true_pi - colMeans(params_per_char$pi)) / true_pi) * 100)
  
  # 1c.  average number of tables
  pi_avg_number_tbl <- mean(params_per_char$pi_num_tables)
  
  # 1e. mean pi coefficient variation
  mean_pi_cv <- mean(pi_summary$pi_coefficient_of_variation)
  
  # 1f. averge percent of coverage
  pi_mean_coverage <- mean(pi_summary$pi_coverage)
  
  #+ whether in the percentile range.
  #summarize over all data set to see how many characters per dataset is contained in the range.
  
  ####################################################################
  ####################################################################
  
  ## loop2-2: compute for rate - each character
  
  rate_summary <- vector("list", length = ncol(params_per_char$rate))
  
  for(j in 1:ncol(params_per_char$rate)) {
    
    # 1a.  average coverage per character (averaged over characters)
    this_true_rate  <- true_rate[n == this_n]
    rate_interval <- quantile(params_per_char$rate[,j], prob=c(0.025,0.975))
    whether_rate_is_contained <- this_true_rate>rate_interval[1] & this_true_rate<rate_interval[2] 
    
    # 1d. coefficient of Variation
    rate_cv <- sd(params_per_char$rate[,j]) / mean(params_per_char$rate[,j])
    
    rate_dtframe = data.frame(rate_coverage = whether_rate_is_contained, rate_coefficient_of_variation = rate_cv)
    
    rate_summary[[j]] = rate_dtframe
  }
  
  rate_summary <- do.call(rbind, rate_summary)
  
  
  pi_summary <- vector("list", length = ncol(params_per_char$pi))
  
  for(j in 1:ncol(params_per_char$pi)) {
    
    # 1a.  average coverage per character (averaged over characters)
    this_true_pi  <- true_rate[n == this_n]
    pi_interval <- quantile(params_per_char$oi[,j], prob=c(0.025,0.975))
    whether_pi_is_contained <- this_true_pi>pi_interval[1] & this_true_pi<pi_interval[2] 
    
    # 1d. coefficient of Variation
    pi_cv <- sd(params_per_char$pi[,j]) / mean(params_per_char$pi[,j])
    
    pi_dtframe = data.frame(pi_coverage = whether_pi_is_contained, pi_coefficient_of_variation = rate_cv)
    
    pi_summary[[j]] = pi_dtframe
  }
  
  pi_summary <- do.call(rbind, pi_summary)
  
  # 1b.  average percent error per character (averaged over characters)
  rate_avg_perc_err <- mean(abs((this_true_rate - colMeans(params_per_char$rate)) / this_true_rate) * 100)
  
  # 1c.  average number of tables
  rate_avg_number_tbl <- mean(params_per_char$rate_num_tables)
  
  # 1e. mean rate coefficient variation
  mean_rate_cv <- mean(rate_summary$rate_coefficient_of_variation)
  
  # 1f. averge percent of coverage
  rate_mean_coverage <- mean(rate_summary$rate_coverage)
  
  all_data_frame = data.frame(
                              pi_percent_error     = pi_avg_perc_err, 
                              pi_number_tbl        = pi_avg_number_tbl, 
                              pi_coeff_variation   = mean_pi_cv, 
                              pi_mean_coverage     = pi_mean_coverage, 
                              rate_percent_error   = rate_avg_perc_err, 
                              rate_number_tbl      = rate_avg_number_tbl, 
                              rate_coeff_variation = mean_rate_cv, 
                              rate_mean_coverage   = rate_mean_coverage)
  
  results[[i]] <- all_data_frame
  
}
results <- do.call(rbind, results)


####################################################################

# repeating previous boxplot code chunk so I'm commenting these out
#pdf(paste0("data/data_summary/", pi_model, "_coverage_summary_pi.pdf"))
#  boxplot(params_per_char$pi, outline = FALSE, ylim = c(0,1))
#  points(rep(0.75, this_c), col = "blue", pch = 19)
#dev.off()


pi_cv <- numeric(ncol(params_per_char$pi))
for(i in 1:ncol(params_per_char$pi)) {
  pi_cv[i] <- sd(params_per_char$pi[,i]) / mean(params_per_char$pi[,i])
}
mean_pi_cv <- mean(pi_cv)

########################## 写表 #################################
# write into a summarizing table.

dir.create(paste0("data/data_summary"))
write.table(results, file = paste0("data/data_summary/", rate_model, "_coverage_summary.tsv"), sep = "\t", quote = FALSE, row.names = FALSE)