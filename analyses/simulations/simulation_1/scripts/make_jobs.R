##### run in simulations/simulation_1 directory #####

# create job directory, if needed.
#dir.create("jobs", showWarnings = FALSE)
out_dir <- "jobs"

# read template
job_template <- readLines("scripts/job_stub.Rev")

# dataset stuff
num_tips <- c(25, 50, 100, 200)
nchar    <- c(50, 100, 200)
rep      <- 1:100

# model stuff
rate_model <- c("constant", "DPP")
pi_model   <- c("constant", "DPP")
tree_model <- c("unrooted")

# number of categories
pi_num_cats   <- 5
rate_num_cats <- 5

# analysis stuff
run_ID      <- 1
burnin      <- 1000
tune_freq   <- 50
generations <- 20000
printgen    <- 5
analysis    <- "mcmc"

# enumerate all combinations
grid <- expand.grid(num_tips         = num_tips, 
                    nchar            = nchar,
                    rep              = rep,
                    rate_model       = rate_model,
                    pi_model         = pi_model,
                    tree_model       = tree_model,
                    pi_num_cats      = pi_num_cats,
                    rate_num_cats    = rate_num_cats,
                    run_ID           = run_ID,
                    burnin           = burnin,
                    tune_freq        = tune_freq,
                    generations      = generations,
                    printgen         = printgen,
                    analysis         = analysis,
                    stringsAsFactors = FALSE)

# remove some combinations
grid <- grid[grid$rate_model == grid$pi_model,]

# loop over grid
for(i in 1:nrow(grid)) {
  
  # get the analysis
  this_analysis <- grid[i,]
  
  # get the settings
  this_num_tips      <- this_analysis$num_tips
  this_nchar         <- this_analysis$nchar
  this_rep           <- this_analysis$rep
  this_rate_model    <- this_analysis$rate_model
  this_pi_model      <- this_analysis$pi_model
  this_tree_model    <- this_analysis$tree_model
  this_pi_num_cats   <- this_analysis$pi_num_cats
  this_rate_num_cats <- this_analysis$rate_num_cats
  this_run_id        <- this_analysis$run_ID
  this_burnin        <- this_analysis$burnin
  this_tune_freq     <- this_analysis$tune_freq
  this_generations   <- this_analysis$generations
  this_printgen      <- this_analysis$printgen
  this_analysis_type <- this_analysis$analysis
  
  # create the job file for the current analysis
  this_job <- job_template
  
  # replace values
  this_job <- gsub("NUM_TIPS",    this_num_tips, this_job)
  this_job <- gsub("REP",         this_rep, this_job)
  this_job <- gsub("NCHAR",       this_nchar, this_job)
  this_job <- gsub("RATE_MODEL",  this_rate_model, this_job)
  this_job <- gsub("PI_MODEL",    this_pi_model, this_job)
  this_job <- gsub("TREE_MODEL",  this_tree_model, this_job)
  this_job <- gsub("PI_CATS",     this_pi_num_cats, this_job)
  this_job <- gsub("RATE_CATS",   this_rate_num_cats, this_job)
  this_job <- gsub("RUN_ID",      this_run_id, this_job)
  this_job <- gsub("BURNIN",      this_burnin, this_job)
  this_job <- gsub("TUNE_FREQ",   this_tune_freq, this_job)
  this_job <- gsub("GENERATIONS", this_generations, this_job)
  this_job <- gsub("PRINTGEN",    this_printgen, this_job)
  this_job <- gsub("ANALYSIS",    this_analysis_type, this_job)
  
  # write the job to file
  this_fn <- paste0(out_dir, "/job_n_", this_num_tips, 
                             "_tree_", this_rep,
                             "_char_", this_nchar, 
                             "_r_", this_rate_model, 
                             "_pi_", this_pi_model, 
                             ".Rev")
  writeLines(this_job, this_fn)  
}




