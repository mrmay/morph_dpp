#!/bin/bash

# synchronize output from savio to local
rsync -a --progress -e ssh eriawei@dtn.brc.berkeley.edu:/global/scratch/users/eriawei/morph_dpp/analyses/empirical/wright/output/ /Users/eriawei/Documents/Repository/morph_dpp/analyses/empirical/wright/output/
