# run from analyses/empirical/wright
# setwd("analyses/empirical/wright/")

library(ape)
library(parallel)
library(viridis)

# prior probability function
cladePriorProb <- function(T, total) {
  
  if (T == total) {
    return(1)
  }
  
  # compute on log scale
  term_1 <- sum(log(2 * 2:T - 3))
  term_2 <- sum(log(2 * (T + 1):total - 2 * T - 1))
  denom  <- sum(log(2 * 2:total - 3))
  
  # return on real scale
  return(exp(term_1 + term_2 - denom))
    
}


# enumerate all the analyses
models <- c("rate_constant_pi_constant_tree_unrooted",
            "rate_DPP_pi_DPP_tree_unrooted",
            "rate_discrete_gamma_pi_discrete_beta_tree_unrooted")
runs <- 1:4

# all combinations of analyses
all_combos <- expand.grid(run = runs, model = models, stringsAsFactors = FALSE)

# get all the directories
all_dirs <- list.files("output", full.names = TRUE)

# generate summaries for all the datasets
invisible(mclapply(all_dirs, function(dir) {

  # progress
  cat(dir, "\n", sep="")  
  
  # read the output
  output <- do.call(rbind, lapply(1:nrow(all_combos), function(i) {
    
    # get the run
    this_analysis <- all_combos[i,]
    this_model    <- this_analysis[["model"]]
    this_run      <- this_analysis[["run"]]
    
    cat(this_model, "\t", this_run, "\n", sep = "")
    
    # get the filename
    this_fn <- paste0(dir, "/", this_model, "/trees_run_", this_run, ".trees")
    
    # check if the output exists
    if (file.exists(this_fn) == FALSE) {
      return(NULL)
    }
    
    # read the trees
    trees <- try(read.tree(this_fn), silent = TRUE)
    if ( class(trees) == "try-error" ) {
      cat("  !!!!Something went wrong with ", dir, " model ", this_model, " run " , this_run, "!!!!\n", sep = "")
      return(NULL) 
    }
    
    # recode the splits with taxon labels
    splits <- prop.part(trees)
    labels <- attr(splits, "label")
    split_names <- lapply(splits, function(x) paste0(sort(labels[x]), collapse = " "))
    
    # compute the frequency of all the splits
    posteriors <- attr(splits, "number") / length(trees)
    names(posteriors) <- split_names
    
    # compute the Bayes factors
    num_taxa           <- length(trees[[1]]$tip.label)
    num_taxa_per_clade <- lengths(splits)
    priors             <- sapply(num_taxa_per_clade, cladePriorProb, total = num_taxa)
    prior_clade_log_odds     <- log(priors)     - log(1 - priors)
    posterior_clade_log_odds <- log(posteriors) - log(1 - posteriors)
    clade_bf <- 2 * as.vector(posterior_clade_log_odds - prior_clade_log_odds)
    names(clade_bf) <- split_names
    
    # return
    res <- data.frame(model  = this_model, 
                      run    = this_run, 
                      trees  = I(list(trees)),
                      splits = I(list(posteriors)),
                      bfs    = I(list(clade_bf)))
    return(res)
    
  }))
  
  if ( nrow(output) != 12 ) {
    return(NULL)
  }
  
  # get all the partitions
  unique_splits <- unique(unlist(lapply(output$splits, names)))
  
  # make a table of frequencies
  split_mat <- matrix(NA, nrow = length(unique_splits), ncol = nrow(all_combos))
  colnames(split_mat) <- apply(all_combos, 1, function(x) paste0(x[2], "_run_", x[1]))
  
  # loop over results
  for(i in 1:nrow(all_combos)) {
    
    # get the output
    this_analysis <- all_combos[i,]
    this_model    <- this_analysis[["model"]]
    this_run      <- this_analysis[["run"]]
    this_output  <- output[output$model == this_model & output$run == this_run,]
    
    # skip if empty
    if ( nrow(this_output) == 0 ) {
      next
    }
    
    # get the splits
    these_splits      <- this_output$splits[[1]]
    these_split_names <- names(these_splits)
    
    # write them down into the table
    split_mat[,i] <- 0
    split_mat[match(these_split_names, unique_splits),i] <- as.numeric(these_splits)
    
  }
  
  # write the table
  split_dir <- paste0(dir, "/splits")
  dir.create(split_dir, recursive = TRUE, showWarnings = FALSE)
  write.table(split_mat, file = paste0(split_dir, "/splits.tsv"), sep = "\t", col.names = TRUE, row.names = FALSE, quote = FALSE)
  
  # make the plot
  colnames(split_mat) <- paste0(rep(c("constant","DPP","discrete"), each = 4), " ", rep(runs, times = 3))
  split_mat <- split_mat[,apply(split_mat, 2, function(x) any(is.na(x))) == FALSE]
  
  pdf(paste0(split_dir,"/splits.pdf"), height = 20, width = 20)
  pairs(split_mat, upper.panel = NULL,
        lower.panel = function(x, y, ...) {
          abline(a = 0, b = 1, lty = 2)
          points(x, y)
        })
  dev.off()
  
  # make a table of bayes factors
  split_mat <- matrix(NA, nrow = length(unique_splits), ncol = nrow(all_combos))
  colnames(split_mat) <- apply(all_combos, 1, function(x) paste0(x[2], "_run_", x[1]))
  
  # loop over results
  for(i in 1:nrow(all_combos)) {
    
    # get the output
    this_analysis <- all_combos[i,]
    this_model    <- this_analysis[["model"]]
    this_run      <- this_analysis[["run"]]
    this_output   <- output[output$model == this_model & output$run == this_run,]
    
    # skip if empty
    if ( nrow(this_output) == 0 ) {
      next
    }
    
    # get the splits
    these_splits      <- this_output$bfs[[1]]
    these_split_names <- names(these_splits)
    
    # write them down into the table
    split_mat[,i] <- -Inf
    split_mat[match(these_split_names, unique_splits),i] <- as.numeric(these_splits)
    
  }
  
  # write the table
  split_dir <- paste0(dir, "/splits")
  dir.create(split_dir, recursive = TRUE, showWarnings = FALSE)
  write.table(split_mat, file = paste0(split_dir, "/bfs.tsv"), sep = "\t", col.names = TRUE, row.names = FALSE, quote = FALSE)
  
  # make the plot
  colnames(split_mat) <- paste0(rep(c("constant","DPP","discrete"), each = 4), " ", rep(runs, times = 3))
  # split_mat <- split_mat[,apply(split_mat, 2, function(x) any(is.na(x))) == FALSE]
  
  pdf(paste0(split_dir,"/bfs.pdf"), height = 20, width = 20)
  pairs(split_mat, upper.panel = NULL,
        lower.panel = function(x, y, ...) {
          abline(a = 0, b = 1, lty = 2)
          points(x, y)
        })
  dev.off()
  
  
}, mc.cores = 6, mc.preschedule = FALSE))
























