#!/bin/bash
#SBATCH --partition=savio
#SBATCH --account=fc_rothfelslab
#SBATCH --qos=savio_normal
#SBATCH --mail-user=eriawei@berkeley.edu
#SBATCH --mail-type=ALL
#SBATCH --time=72:00:00


# load dependencies
module load rb

# change to the repo directory
cd /global/scratch/users/eriawei/morph_dpp/analyses/empirical/wright/


rb jobs/job_Adrain_et_Ramskold_1997a_r_discrete_gamma_pi_discrete_beta_ID_3_summarize.Rev & 
rb jobs/job_Adrain_et_Ramskold_1997a_r_discrete_gamma_pi_discrete_beta_ID_4_summarize.Rev ;

wait;


