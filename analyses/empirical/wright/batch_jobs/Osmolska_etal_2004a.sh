#!/bin/bash
#SBATCH --partition=savio
#SBATCH --account=fc_rothfelslab
#SBATCH --qos=savio_normal
#SBATCH --mail-user=eriawei@berkeley.edu
#SBATCH --mail-type=ALL
#SBATCH --time=72:00:00


# load dependencies
module load rb

# change to the repo directory
cd /global/scratch/users/eriawei/morph_dpp/analyses/empirical/wright/

rb jobs/job_Osmolska_etal_2004a_r_discrete_gamma_pi_discrete_beta_ID_1.Rev & 
rb jobs/job_Osmolska_etal_2004a_r_constant_pi_constant_ID_1.Rev &
rb jobs/job_Osmolska_etal_2004a_r_DPP_pi_DPP_ID_1.Rev &
rb jobs/job_Osmolska_etal_2004a_r_discrete_gamma_pi_discrete_beta_ID_2.Rev & 
rb jobs/job_Osmolska_etal_2004a_r_constant_pi_constant_ID_2.Rev &
rb jobs/job_Osmolska_etal_2004a_r_DPP_pi_DPP_ID_2.Rev &
rb jobs/job_Osmolska_etal_2004a_r_discrete_gamma_pi_discrete_beta_ID_3.Rev & 
rb jobs/job_Osmolska_etal_2004a_r_constant_pi_constant_ID_3.Rev &
rb jobs/job_Osmolska_etal_2004a_r_DPP_pi_DPP_ID_3.Rev &
rb jobs/job_Osmolska_etal_2004a_r_discrete_gamma_pi_discrete_beta_ID_4.Rev & 
rb jobs/job_Osmolska_etal_2004a_r_constant_pi_constant_ID_4.Rev &
rb jobs/job_Osmolska_etal_2004a_r_DPP_pi_DPP_ID_4.Rev ;

wait;


