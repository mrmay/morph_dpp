#!/bin/bash
#SBATCH --partition=savio
#SBATCH --account=fc_rothfelslab
#SBATCH --qos=savio_normal
#SBATCH --mail-user=eriawei@berkeley.edu
#SBATCH --mail-type=ALL
#SBATCH --time=72:00:00


# load dependencies
module load rb

# change to the repo directory
cd /global/scratch/users/eriawei/morph_dpp/analyses/empirical/marattiales/

rb jobs/m_DPP_4.Rev &
rb jobs/m_DPP_3.Rev &
rb jobs/m_DPP_2.Rev &
rb jobs/m_DPP_1.Rev &
rb jobs/m_constant_1.Rev &
rb jobs/m_constant_2.Rev &
rb jobs/m_constant_3.Rev &
rb jobs/m_constant_4.Rev &
rb jobs/m_discrete_1.Rev &
rb jobs/m_discrete_2.Rev &
rb jobs/m_discrete_3.Rev &
rb jobs/m_discrete_4.Rev ;

wait;


