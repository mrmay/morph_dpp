library(convenience)
setwd("Documents/Repository/morph_dpp/analyses/empirical/marrattiales/")

# output directory
dir <- "output/Marattiales/"

# create plot directory for the dataset
plot_dir <- paste0("output/", "/plots")
dir.create(plot_dir, showWarnings = TRUE)

# read samples from each model
constant_dir       <- paste0(dir, "rate_constant_pi_constant_tree_unrooted")
constant_tree_file <- list.files(constant_dir, pattern = "run_1.trees", full.names = TRUE)

discrete_dir       <- paste0(dir, "rate_discrete_gamma_pi_discrete_beta_tree_unrooted")
discrete_tree_file <- list.files(discrete_dir, pattern = "run_1.trees", full.names = TRUE)

DPP_dir       <- paste0(dir, "rate_DPP_pi_DPP_tree_unrooted")
DPP_tree_file <- list.files(DPP_dir, pattern = "run_1.trees", full.names = TRUE)

# compare constant vs. DPP
constant_vs_dpp <- checkConvergence(list_files = c(constant_tree_file, DPP_tree_file))
pdf(paste0("output/plots/constant_vs_dpp_tree_unrooted_compare.pdf"))
plotDiffSplits(constant_vs_dpp, minimumESS = 200)
dev.off()

# compare constant vs. discrete
constant_vs_discrete <- checkConvergence(list_files = c(constant_tree_file, discrete_tree_file))
pdf(paste0("output/", dataset, "/plots/", dataset, "_constant_vs_discrete_tree_unrooted_compare.pdf"))
plotDiffSplits(constant_vs_discrete, minimumESS = 200)
dev.off()

# compare discrete vs. DPP
discrete_vs_DPP <- checkConvergence(list_files = c(discrete_tree_file, DPP_tree_file))
pdf(paste0("output/", dataset, "/plots/", dataset, "_discrete_vs_DPP_tree_unrooted_compare.pdf"))
plotDiffSplits(discrete_vs_DPP, minimumESS = 200)
dev.off()



